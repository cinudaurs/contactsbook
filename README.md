# README #

### What is this repository for? ###

* Demonstrate use of a CursorLoader with a LoaderManager Instead of directly querying from with in MainActivity (i.e on gui thread of execution)
* Use content provider to access data in contactsbook asyncly, so that we don't have long running db operations on gui thread of execution.
* Use AsyncTask, Instead of a Runnable for background tasks, so the gui thread of execution is not blocked.
* Demonstrate use of AsyncTask.THREAD_POOL_EXECUTOR to run tasks parallelly using:
  mSaveToDBTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR); where mSaveToDBTask is the AsyncTask which does the operation of insert for each Content   
  Value in overriden doInBackground method call.