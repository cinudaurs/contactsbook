package com.example.srini.contactsbook;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements ContactsFragment.ContactsFragmentListener,
        EditFragment.EditFragmentListener {


    // key for storing a contact's Uri in a Bundle passed to a fragment
    public static final String CONTACT_URI = "contact_uri";

    private ContactsFragment contactsFragment; // displays contact list

    // display ContactsFragment when MainActivity first loads
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // if layout contains fragmentContainer, the phone layout is in use;
        // create and display a ContactsFragment
        if (savedInstanceState == null &&
                findViewById(R.id.fragmentContainer) != null) {
            // create ContactsFragment
            contactsFragment = new ContactsFragment();

            // add the fragment to the FrameLayout
            //AppCompatActivity gives SupportFragmentManager.
            //If it was Activity, this would be FragmentManager.
            FragmentTransaction transaction =
                    getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.fragmentContainer, contactsFragment);
            transaction.commit(); // display ContactsFragment
        }
        else {
            contactsFragment =
                    (ContactsFragment) getSupportFragmentManager().
                            findFragmentById(R.id.contactsFragment);
        }
    }

    // display EditFragment to add a new contact
    @Override
    public void onEditContacts() {
        if (findViewById(R.id.fragmentContainer) != null) // phone
            displayEditFragment(R.id.fragmentContainer, null);
    }

    // display fragment for adding a new or editing an existing contact
    private void displayEditFragment(int viewID, Uri contactUri) {

        EditFragment editFragment = new EditFragment();

        // if editing existing contact, provide contactUri as an argument
        if (contactUri != null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(CONTACT_URI, contactUri);
            editFragment.setArguments(arguments);
        }

        // use a FragmentTransaction to display the EditFragment
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.replace(viewID, editFragment);
        transaction.addToBackStack(null);
        transaction.commit(); // causes EditFragment to display
    }


    @Override
    public void onAddEditCompleted(Uri newContactUri) {
        getSupportFragmentManager().popBackStack();
        contactsFragment.updateContactList(); // refresh contacts
    }
}
