package com.example.srini.contactsbook;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.example.srini.contactsbook.data.DatabaseDescription.Contact;

/**
 * Created by srini on 8/1/16.
 */
public class EditFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor> {


    // constant used to identify the Loader
    private static final int CONTACT_LOADER = 0;

    private EditFragmentListener listener; // MainActivity
    private Uri contactUri; // Uri of selected contact
    private boolean addingNewContact = true; // adding (true) or editing

    // EditTexts for contact information
    private TextInputLayout nameTextInputLayout1;
    private TextInputLayout nameTextInputLayout2;
    private TextInputLayout nameTextInputLayout3;
    private TextInputLayout nameTextInputLayout4;
    private TextInputLayout nameTextInputLayout5;
    private TextInputLayout nameTextInputLayout6;
    private TextInputLayout nameTextInputLayout7;
    private FloatingActionButton saveContactFAB;

    private CoordinatorLayout coordinatorLayout; // used with SnackBars

    SaveToDBTask mSaveToDBTask1;
    SaveToDBTask mSaveToDBTask2;
    SaveToDBTask mSaveToDBTask3;
    SaveToDBTask mSaveToDBTask4;
    SaveToDBTask mSaveToDBTask5;
    SaveToDBTask mSaveToDBTask6;
    SaveToDBTask mSaveToDBTask7;


    // set AddEditFragmentListener when Fragment attached
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (EditFragmentListener) context;
    }

    // remove AddEditFragmentListener when Fragment detached
    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    // detects when the text in the nameTextInputLayout's EditText changes
    // to hide or show saveButtonFAB
    private final TextWatcher nameChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        // called when the text in nameTextInputLayout changes
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            updateSaveButtonFAB();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    // saves contacts information to the database
    private void saveContacts() {
        // create ContentValues object containing contact's key-value pairs
        ContentValues contentValues1 = new ContentValues();
        ContentValues contentValues2 = new ContentValues();
        ContentValues contentValues3 = new ContentValues();
        ContentValues contentValues4 = new ContentValues();
        ContentValues contentValues5 = new ContentValues();
        ContentValues contentValues6 = new ContentValues();
        ContentValues contentValues7 = new ContentValues();


        contentValues1.put(Contact.COLUMN_NAME,
                nameTextInputLayout1.getEditText().getText().toString());

        contentValues2.put(Contact.COLUMN_NAME,
                nameTextInputLayout2.getEditText().getText().toString());

        contentValues3.put(Contact.COLUMN_NAME,
                nameTextInputLayout3.getEditText().getText().toString());

        contentValues4.put(Contact.COLUMN_NAME,
                nameTextInputLayout4.getEditText().getText().toString());

        contentValues5.put(Contact.COLUMN_NAME,
                nameTextInputLayout5.getEditText().getText().toString());

        contentValues6.put(Contact.COLUMN_NAME,
                nameTextInputLayout6.getEditText().getText().toString());

        contentValues7.put(Contact.COLUMN_NAME,
                nameTextInputLayout7.getEditText().getText().toString());


        mSaveToDBTask1 = new SaveToDBTask(getActivity().getBaseContext(), contentValues1);
        mSaveToDBTask2 = new SaveToDBTask(getActivity().getBaseContext(), contentValues2);
        mSaveToDBTask3 = new SaveToDBTask(getActivity().getBaseContext(), contentValues3);
        mSaveToDBTask4 = new SaveToDBTask(getActivity().getBaseContext(), contentValues4);
        mSaveToDBTask5 = new SaveToDBTask(getActivity().getBaseContext(), contentValues5);
        mSaveToDBTask6 = new SaveToDBTask(getActivity().getBaseContext(), contentValues6);
        mSaveToDBTask7 = new SaveToDBTask(getActivity().getBaseContext(), contentValues7);

        StartAsyncTaskInParallel(mSaveToDBTask1);
        StartAsyncTaskInParallel(mSaveToDBTask2);
        StartAsyncTaskInParallel(mSaveToDBTask3);
        StartAsyncTaskInParallel(mSaveToDBTask4);
        StartAsyncTaskInParallel(mSaveToDBTask5);
        StartAsyncTaskInParallel(mSaveToDBTask6);
        StartAsyncTaskInParallel(mSaveToDBTask7);


    }

    private void StartAsyncTaskInParallel(SaveToDBTask mSaveToDBTask) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            mSaveToDBTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            mSaveToDBTask.execute();
    }


    // called when Fragment's view needs to be created
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true); // fragment has menu items to display

        // inflate GUI and get references to EditTexts
        View view =
                inflater.inflate(R.layout.fragment_add_edit, container, false);
        nameTextInputLayout1 =
                (TextInputLayout) view.findViewById(R.id.nameTextInputLayout);
        nameTextInputLayout1.getEditText().addTextChangedListener(
                nameChangedListener);

        nameTextInputLayout2 =
                (TextInputLayout) view.findViewById(R.id.nameTextInputLayout2);
        nameTextInputLayout2.getEditText().addTextChangedListener(
                nameChangedListener);

        nameTextInputLayout3 =
                (TextInputLayout) view.findViewById(R.id.nameTextInputLayout3);
        nameTextInputLayout3.getEditText().addTextChangedListener(
                nameChangedListener);

        nameTextInputLayout4 =
                (TextInputLayout) view.findViewById(R.id.nameTextInputLayout4);
        nameTextInputLayout4.getEditText().addTextChangedListener(
                nameChangedListener);

        nameTextInputLayout5 =
                (TextInputLayout) view.findViewById(R.id.nameTextInputLayout5);
        nameTextInputLayout5.getEditText().addTextChangedListener(
                nameChangedListener);

        nameTextInputLayout6 =
                (TextInputLayout) view.findViewById(R.id.nameTextInputLayout6);
        nameTextInputLayout6.getEditText().addTextChangedListener(
                nameChangedListener);

        nameTextInputLayout7 =
                (TextInputLayout) view.findViewById(R.id.nameTextInputLayout7);
        nameTextInputLayout7.getEditText().addTextChangedListener(
                nameChangedListener);

        // set FloatingActionButton's event listener
        saveContactFAB = (FloatingActionButton) view.findViewById(
                R.id.saveFloatingActionButton);
        saveContactFAB.setOnClickListener(saveContactButtonClicked);
        updateSaveButtonFAB();

        // used to display SnackBars with brief messages
        coordinatorLayout = (CoordinatorLayout) getActivity().findViewById(
                R.id.coordinatorLayout);

        Bundle arguments = getArguments(); // null if creating new contact

        if (arguments != null) {
            addingNewContact = false;
            contactUri = arguments.getParcelable(MainActivity.CONTACT_URI);
        }

        // if editing an existing contact, create Loader to get the contact
        if (contactUri != null)
            getLoaderManager().initLoader(CONTACT_LOADER, null, this);

        return view;
    }

    // shows saveButtonFAB only if the name is not empty
    private void updateSaveButtonFAB() {

        String input1 =
                nameTextInputLayout1.getEditText().getText().toString();

        String input2 =
                nameTextInputLayout2.getEditText().getText().toString();

        String input3 =
                nameTextInputLayout3.getEditText().getText().toString();

        String input4 =
                nameTextInputLayout4.getEditText().getText().toString();

        String input5 =
                nameTextInputLayout5.getEditText().getText().toString();

        String input6 =
                nameTextInputLayout6.getEditText().getText().toString();

        String input7 =
                nameTextInputLayout6.getEditText().getText().toString();

        // if there is a name for the contact, show the FloatingActionButton
        if (input1.trim().length() != 0 || input2.trim().length() != 0 || input3.trim().length() != 0 || input4.trim().length() != 0 || input5.trim().length() != 0 ||
                input6.trim().length() != 0 || input7.trim().length() != 0)
            saveContactFAB.show();
        else
            saveContactFAB.hide();
    }

    // responds to event generated when user saves a contact
    private final View.OnClickListener saveContactButtonClicked =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // hide the virtual keyboard
                    ((InputMethodManager) getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
                            getView().getWindowToken(), 0);
                    saveContacts(); // save contact to the database
                }
            };


    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // create an appropriate CursorLoader based on the id argument;
        // only one Loader in this fragment, so the switch is unnecessary
        switch (id) {
            case CONTACT_LOADER:
                return new CursorLoader(getActivity(),
                        contactUri, // Uri of contact to display
                        null, // null projection returns all columns
                        null, // null selection returns all rows
                        null, // no selection arguments
                        null); // sort order
            default:
                return null;
        }
    }

    // called by LoaderManager when loading completes
    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
    }


    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {

    }

    // defines callback method implemented by MainActivity
    public interface EditFragmentListener {

        void onAddEditCompleted(Uri newContactUri);
    }


    private class SaveToDBTask extends AsyncTask<Void, Void, Void> {


        private Context context;
        private ContentValues contentValues;

        public SaveToDBTask(Context baseContext, ContentValues contentValues) {
            this.context = baseContext;
            this.contentValues = contentValues;
        }


        @Override
        protected Void doInBackground(Void... params) {

            Uri newContactUri = getActivity().getContentResolver().insert(
                    Contact.CONTENT_URI, contentValues);

            if (newContactUri != null) {
                Snackbar.make(coordinatorLayout,
                        R.string.contact_added, Snackbar.LENGTH_LONG).show();
//                listener.onAddEditCompleted(newContactUri);
            } else {
                Snackbar.make(coordinatorLayout,
                        R.string.contact_not_added, Snackbar.LENGTH_LONG).show();
            }


            return null;
        }
    }
}
